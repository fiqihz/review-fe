import { AddKontak, ListKontak } from "./Component";

function App() {
  return (
    <div style={{ padding: "20px" }}>
      <h2>Aplikasi List Kontak</h2>
      <hr />
      <AddKontak />
      <hr />
      <ListKontak />
    </div>
  );
}

export default App;
