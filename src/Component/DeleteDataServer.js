import React, { useState } from "react";
import axios from "axios";

function DeleteDataServer() {
  const deleteData = () => {
    const data = {
      userId: 1,
      id: 1,
      title:
        "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
    };
    axios
      .delete("https://jsonplaceholder.typicode.com/posts/1", data)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Delete Data dari Server</h2>
      <button onClick={deleteData}>Delete Data</button>
    </div>
  );
}

export default DeleteDataServer;
