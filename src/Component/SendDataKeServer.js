import React, { useState } from "react";
import axios from "axios";

function SendDataKeserver() {
  const [form, setForm] = useState({
    userId: "",
    title: "",
    body: "",
  });

  const eventHandler = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    console.log(form);
    axios
      .post("https://jsonplaceholder.typicode.com/posts", form)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Send Data ke Server</h2>
      <input
        type="text"
        name="userId"
        placeholder="User ID"
        onChange={eventHandler}
      />
      <br />
      <input
        type="text"
        name="title"
        placeholder="Title"
        onChange={eventHandler}
      />
      <br />
      <input
        type="text"
        name="body"
        placeholder="Body"
        onChange={eventHandler}
      />
      <br />
      <button type="submit" onClick={submitHandler}>
        Submit
      </button>
    </div>
  );
}

export default SendDataKeserver;
