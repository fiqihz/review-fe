import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addKontak, getListKontak } from "../../Action/kontakAction";

function AddKontak() {
  const [nama, setNama] = useState("");
  const [nohp, setNohp] = useState("");

  const { addKontakResult } = useSelector((state) => state.KontakReducer);
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(addKontak({ nama: nama, nohp: nohp }));
  };

  useEffect(() => {
    if (addKontakResult) {
      dispatch(getListKontak());
      setNama("");
      setNohp("");
    }
  }, [addKontakResult, dispatch]);

  return (
    <div>
      <h4>Add Kontak</h4>
      <form onSubmit={(event) => handleSubmit(event)}>
        <input
          type="text"
          name="nama"
          placeholder="nama"
          value={nama}
          onChange={(event) => setNama(event.target.value)}
        ></input>
        <br />
        <input
          type="text"
          name="nohp"
          placeholder="No. HP"
          value={nohp}
          onChange={(event) => setNohp(event.target.value)}
        ></input>
        <br />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default AddKontak;
