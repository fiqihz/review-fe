import React, { useState, useEffect } from "react";
import axios from "axios";

function GetDataDariServer() {
  const [post, setPost] = useState([]);
  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        setPost(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  return (
    <div>
      <h2>Get Data dari Server</h2>
      <ul>
        {post.map((post) => (
          <li key={post.id}>
            id: {post.id}, title: {post.title}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default GetDataDariServer;
