import React, { useState } from "react";
import axios from "axios";

function UpdateDateKeServer() {
  const [form, setForm] = useState({
    userId: "",
    title: "",
    body: "",
  });

  const eventHandler = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const updateHandler = (event) => {
    event.preventDefault();
    console.log(form);
    axios
      .put("https://jsonplaceholder.typicode.com/posts/1", form)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h2>Hello from update</h2>
      <input
        type="text"
        name="userId"
        placeholder="User ID"
        onChange={eventHandler}
      />
      <br />
      <input
        type="text"
        name="title"
        placeholder="Title"
        onChange={eventHandler}
      />
      <br />
      <input
        type="text"
        name="body"
        placeholder="Body"
        onChange={eventHandler}
      />
      <br />
      <button type="submit" onClick={updateHandler}>
        Update
      </button>
    </div>
  );
}

export default UpdateDateKeServer;
